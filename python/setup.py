from setuptools import setup

setup(name='dune.docker',
      namespace_packages=['dune'],
      version='2.4',
      description='Python package for working with the DUNE project and docker',
      url='http://www.dune-project.org',
      author='Dominic Kempf',
      author_email='dominic.kempf@iwr.uni-heidelberg.de',
      license='BSD',
      install_requires=['docker-py',
                        'asyncio',
                        'click',
                        ],
      packages=['dune.docker',
                ],
      entry_points={
          'console_scripts': [
             'build_images=dune.docker.build:run',
             'push_images=dune.docker.push:run',
          ],
      },
      )
