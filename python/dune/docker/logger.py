"""
A parser for the output from dockerpy
"""

from dune.citools.logging.splitter import LogSplitterBase
from dune.citools.logging.writer import StdoutLogWriter, FileLogWriter


class DockerError(Exception):
    pass


class DockerBuildLogSplitter(LogSplitterBase):

    def __init__(self, image):
        LogSplitterBase.__init__(self)
        self.image = image

        image = image.replace('/', '_').replace(':', '_')

        # Initialize standard writers for docker build logs
        self.register_log(StdoutLogWriter())
        self.register_log(FileLogWriter("image", image + "_build.log"))
        self.register_log(FileLogWriter("image_error", image + "_error_build.log"))

    def parseString(self, line):
        d = dict(line)
        if "stream" in d:
            yield ("image",), d["stream"]
        elif "error" in d:
            yield ("stdout", "image", "image_error"), d["error"]
            raise DockerError("Building of image {} failed:\n{}".format(self.image, d["error"]))
        else:
            raise ValueError("What kind of docker log is that? ({})".format(d))


class DockerPushLogSplitter(LogSplitterBase):

    def __init__(self):
        LogSplitterBase.__init__(self)

        self.register_log(StdoutLogWriter())
        self.register_log(FileLogWriter("push", "pushing.log"))

    def parseString(self, line):
        d = eval(line)
        if "status" in d:
            yield ("push",), d["status"]
        elif "error" in d:
            yield ("push", "stdout"), d["error"]
            raise DockerError("Error while pushing images!")
        elif "aux" in d:
            pass
        else:
            raise ValueError("What kind of docker log is that? ({})".format(d))
