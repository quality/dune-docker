import click
import docker

from dune.docker.image import glob_images
from dune.docker.logger import DockerPushLogSplitter
from dune.citools.logging.context import log_dir


@click.command()
@click.option('--docker-daemon', type=str, default='unix://var/run/docker.sock', help='The URL of the docker daemon')
@click.option('--path', type=str, default='./build', help='The directory to glob for Dockerfiles (recursively)')
def run(docker_daemon, path):
    with log_dir('./logs'):
        with DockerPushLogSplitter() as logsplitter:
            cli = docker.Client(base_url=docker_daemon, version='auto')
            for image in glob_images(path):
                logsplitter.log_from_generator(cli.push(image.name, stream=True))
