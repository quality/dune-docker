import asyncio
from concurrent.futures import ThreadPoolExecutor
import click
import docker
import os

from dune.docker.logger import DockerBuildLogSplitter
from dune.docker.image import DockerImage, get_baseimage, glob_images


def build_image(image, ci_logging, base_url='unix://var/run/docker.sock'):
    # Get a docker client
    cli = docker.Client(base_url=base_url, version='auto')

    build_gen = cli.build(path=image.path, tag=image.name, decode=True)
    if ci_logging:
        with DockerBuildLogSplitter(image.name) as logsplitter:
            logsplitter.log_from_generator(build_gen)
    else:
        for line in build_gen:
            print(line)

@asyncio.coroutine
def task_build_image(image, ci_logging, closure=[]):
    loop = asyncio.get_event_loop()
    if len(closure) > 0:
        yield from asyncio.wait(closure, loop=loop)
    yield from loop.run_in_executor(None, build_image, image, ci_logging)


@click.command()
@click.option('--docker-daemon', type=str, default='unix://var/run/docker.sock', help='The URL of the docker daemon')
@click.option('--processors', type=int, default=os.environ.get("DUNE_DOCKER_MAX_PROCESSORS", 1), help='The number of processors to use to build images in parallel')
@click.option('--path', type=str, default='./build', help='The directory to glob for Dockerfiles (recursively)')
@click.option('--ci-logging', is_flag=True, default=False, help='Whether the log files should be split in a CI-friendly fashion')
def run(docker_daemon, processors, path, ci_logging):
    from dune.citools.logging.context import log_dir
    with log_dir('./logs'):
        # Gather the image list by globbing through the given path
        images = [i for i in glob_images(path)]

        baseimages = {}
        for i in images:
            base = get_baseimage(i)
            if base in images:
                baseimages[i] = base

        closure = {i:[] for i in images}
        for i in baseimages:
            current = i
            while current in baseimages:
                closure[i].append(baseimages[current])
                current = baseimages[current]

        # Set up the asyncio infrastructure
        loop = asyncio.get_event_loop()
        executor = ThreadPoolExecutor(max_workers=processors)
        loop.set_default_executor(executor)

        tasks = {}
        for i in sorted(images, key=lambda i: len(closure[i])):
            tasks[i] = asyncio.async(task_build_image(i, ci_logging, closure=[tasks[b] for b in closure[i]]), loop=loop)

        loop.run_until_complete(asyncio.gather(*tasks.values()))
