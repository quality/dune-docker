import os
import re


class DockerImage:
    """ A data structure that allows to treat a docker image
    either by its name or by the path of its Dockerfile """

    def __init__(self, name=None, path=None):
        assert(name or path)
        assert(not(name and path))

        if path:
            # For consistency, make all paths absolute
            if not os.path.isabs(path):
                path = os.path.abspath(path)
            self.path = path
            # TODO the 'build' subdir in here is very specific to our setup!
            self.name = path.split(os.getcwd() + '/build/', 1)[1]

            # Format name correctly as user/repository:tag
            components = re.split('/|:', self.name)
            assert(len(components) == 3)
            self.name = '{}/{}:{}'.format(*components)
        if name:
            self.path = os.path.join(os.getcwd(), 'build', name)
            self.name = name

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<{}>".format(self.name)

    def __eq__(self, x):
        return self.name == x.name

    def __hash__(self):
        return hash(self.name)


def get_baseimage(image):
    """ Return the baseimage of the given image. Requires inspection
    of the Dockerfile
    """

    f = open(os.path.join(image.path, 'Dockerfile'), 'r')
    for line in f:
        if line.startswith('FROM'):
            return DockerImage(name=line.split()[1])

    # TODO write a custom error class
    raise ValueError("The given Dockerfile ({}) does not specify a baseimage".format(image.name))


def glob_images(path):
    for root, _, fn in os.walk(path):
        if 'Dockerfile' in fn:
            yield DockerImage(path=root)
