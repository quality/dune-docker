Copyright holders:

2015    Dominic Kempf
2015    Timo Koch

The module dune-docker is free software and documentation.

You can use, modify and/or redistribute the contained software (in the directory
src/) under the terms of the BSD 3-clause license, which you can find in the
file BSD.
