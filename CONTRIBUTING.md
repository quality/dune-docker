How to contribute
=================

* Make sure you familiarize yourself with the custom Dockerfile `INCLUDE` directive used in this project
  by reading through `README`.

* Please open merge requests at http://gitlab.dune-project.org/quality/dune-docker
