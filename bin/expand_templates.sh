#!/bin/bash

# All error in this script should be fatal to avoid false commits
set -e

# Remove all previous expansion
rm -rf build

# Gather the list of Dockerfile templates
list=$(find ./images -name '*.template')

for f in $list
do
  # Process the file f
  echo "Processing the Dockerfile template at $f"
  path=$(basename $f | cut -d'.' -f1 | tr "-" "/")
  path=./build/$path
  mkdir -p $path
  docker=$path/Dockerfile
  echo "Writing it to Dockerfile at $docker"

  # For documentation on the read builtin see
  # http://wiki.bash-hackers.org/commands/builtin/read
  # It has some pitfalls to avoid the stripping of
  # whitespace (which breaks the Dockerfiles).
  while read -r || [[ -n "$REPLY" ]]
  do
    if [[ $REPLY == INCLUDE* ]]
    then
      cat "./includes/${REPLY#INCLUDE }.docker" >> $docker
    else
      printf "%s\n" "$REPLY" >> $docker
    fi
  done < "$f"
done
