#!/bin/bash

set -e

virtualenv -p $(which python3) env >> logs/setup.log
source env/bin/activate

# Install dune-citools
git clone http://conan2.iwr.uni-heidelberg.de/git/dominic/dune-citools.git >> logs/setup.log
pip install ./dune-citools >> logs/setup.log
rm -rf dune-citools

pip install -e python >> logs/setup.log
$@

rm -rf env
