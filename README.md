General Information
===================

This repository defines Docker images for the
[Dune project](www.dune-project.org).

Use cases for these are:

* Sandbox environments for reproducible simulations
* Easy modelling of heterogeneous environments in automated testing

Usage
=====

Have a look at the scipts in the `bin` subfolder. There is one
script per typical use case. Scripts should be called from the module
root directory, with `./bin/<scriptname>`.

Manual Usage
------------
To have `Dockerfile`s ready to build images, you need to call
`./bin/expand_templates.sh`
You will then find the `build` subfolder populated with
ready to use `Dockerfile`s.

You can build any docker image from this repository with

`docker build --tag=<imagename> <path_to_dockerfile>`

You can run such container interactively by doing 

`docker run -t -i <imagename>`

Note you need sudo rights for most docker operations.

Automatic usage
---------------

This repository also serves as source for automated docker image
builds. These builds happen on the [dockerhub](https://hub.docker.com/)
and on some private docker registrys.

If images are built into a docker registry, you should use the image
names from the respective README files. That makes the registry interchangeable.

As the docker hub account for the Dune project is called `duneproject`,
you should use this as an account name in private registries to. You can
then set the `--add-registry` option for your docker daemon to a local
registry to have the docker hub registry act as a fallback to potential
local registries. This is how the local buildslave setup in Heidelberg does it.

Developing docker images for Dune (IMPORTANT)
=============================================

Note, that Dockerfiles do *not* provide an `INCLUDE` command. However,
we found ourselves defining the same layers of commands over and over,
so we decided to go for a cheap implementation of the command.

You only develop the following parts in this repository:
* Dockerfile snippets in the `include` subdirectory
* `<name>.template` files in the `images` subdirectory. These templates
  are normal Dockerfiles, extend by `INCLUDE` commands. `INCLUDE` may refer
  to any file in the include directory.
* The `<name>` is used to determine the location of the Dockerfile to be
  generated. Hyphens are replaced with slashed and describe nested structure.
* The actual Dockerfile is written by `bin/expand_templates.sh` into a
  subdirectory `build`.
